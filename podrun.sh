
podman run \
  -ti \
  --name dev_resing \
  -p 9001:9000 \
  -e "ADD_MODULES=mkdocs-bootstrap mkdocs-gitbook mkdocs-bootstrap4" \
  -e "LIVE_RELOAD_SUPPORT=true" \
  -e "FAST_MODE=true" \
  -e "DOCS_DIRECTORY=/my_docs" \
  -e "GIT_REPO=https://codeberg.org/rem/docs.git" \
  -e "GIT_BRANCH=main" \
  -e "AUTO_UPDATE=1" \
  -e "UPDATE_INTERVAL=60" \
  -e "DEV_ADDR=0.0.0.0:9000" \
  polinux/mkdocs

