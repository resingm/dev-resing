#!/bin/bash

pod=dev-resing
ports=9001:80

#
# Pull latest changes
#
echo "Pulling latest changes"
git pull
echo "Updated repository."


#
# Stopping pods
#
echo "Stopping $pod"
podman stop $pod
echo "Removing container"
podman rm $pod

#
# Build & deployment of pod
#
echo "Building pod $pod"
podman build -t $pod .

echo "Starting pod $pod"
podman run --name $pod -p $ports -d $pod

echo "Deployment done."
exit 0
